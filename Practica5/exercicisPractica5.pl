%Problema A, not finished

subcjto([],[]). %subcjto(L,S) es: "S es un subconjunto de L".
subcjto([X|C],[X|S]):-  subcjto(C,S).
subcjto([_|C],S)    :-  subcjto(C,S).

cifras(L,N):- subcjto(L,S), permutation(S,P), expresion(P,E), N is E, write(E),nl,fail.
expresion([X],X).
expresion(L,E1+E2):- append(L1,L2,L), L1\=[],L2\=[], expresion(L1,E1), expresion(L2,E2).
expresion(L,E1-E2):- append(L1,L2,L), L1\=[],L2\=[], expresion(L1,E1), expresion(L2,E2).
expresion(L,E1*E2):- append(L1,L2,L), L1\=[],L2\=[], expresion(L1,E1), expresion(L2,E2).
expresion(L,E1/E2):- append(L1,L2,L), L1\=[],L2\=[], expresion(L1,E1), expresion(L2,E2), E2 > 0.


%=========================================================================================================


nat(0).
nat(N):- nat(N1), N is N1 + 1.

%Problema B1

inverso([],[]).
inverso(L,[X|L1]):- append(L2,[X],L), inverso(L2,L1).

mis:- camino( [lado1,3,3], [lado2,0,0], [[lado1,3,3]] ).

camino(Fin,Fin,Cam):- inverso(Cam,Sol), write(Sol), nl.
camino(Ini,Fin,Cam):- paso(Ini,E), novisitado(E,Cam), camino(E,Fin,[E|Cam]).

novisitado(E,Cam):- member(E,Cam), !,fail.
novisitado(_,_).

paso( [lado1,M1,C1], [lado2,M2,C2] ):- pasan(M,C), M2 is M1-M, C2 is C1-C, safe(M2,C2).
paso( [lado2,M1,C1], [lado1,M2,C2] ):- pasan(M,C), M2 is M1+M, C2 is C1+C, safe(M2,C2).

pasan(M,C):- member( [M,C], [ [0,1], [0,2], [1,0], [1,1], [2,0] ] ).

safe(M,C):- M>=0, M=<3, C>=0, C=<3, nocomen( M, C),
            M1 is 3-M,  C1 is 3-C,  nocomen(M1,C1).

nocomen(0,_).
nocomen(M,C):- M>=C.


%Problema B2, more optimized

camino(E,E, C,C, _,0).
camino(EstadoActual,EstadoFinal, CaminoHastaAhora,CaminoTotal, Tam, Pasos):-
    Pasos > 0,
    unPaso(EstadoActual, EstSiguiente,Tam),
    \+member(EstSiguiente,CaminoHastaAhora),
    Pasos1 is Pasos-1,
    camino(EstSiguiente,EstadoFinal, [EstSiguiente|CaminoHastaAhora],CaminoTotal, Tam, Pasos1)
.

solucionOptima:-
    nat(N),
    camino([0,0],[8,8], [[0,0]],C,9,N),
    N1 is N+1, length(C,N1),                        
    reverse(CInvertido,C), 
    write("Optimal solution found with cost "), write(N1), write(": "), 
    write(CInvertido), nl, !.


unPaso(Ea, Es, Tam):-
    getOpciones(Ea,Tam,L),
    member(Es,L)
.

getOpciones([I,J], T, List):-
    findall([X,Y],
    (
        (X is I+1, X<T,  ((Y is J-2, Y>=0);(Y is J+2, Y<T)));
        (X is I-1, X>=0, ((Y is J-2, Y>=0);(Y is J+2, Y<T)));
        (X is I+2, X<T,  ((Y is J-1, Y>=0);(Y is J+1, Y<T)));
        (X is I-2, X>=0, ((Y is J-1, Y>=0);(Y is J+1, Y<T)))
    ),
    List)
.


%Problema B2, finished but long

goUp2(_,[Y,Ini],Res):- Y1 is Y-2, Y1 >= 0, append([Y1],[Ini],Res). 

goDown2(Tam,[Y,Ini],Res):- Y1 is Y+2, Y1 =< Tam, append([Y1],[Ini],Res). 

goLeft2(_,[Ini,X],Res):- X1 is X-2, X1 >= 0, append([Ini],[X1],Res). 

goRight2(Tam,[Ini,X],Res):- X1 is X+2, X1 =< Tam, append([Ini],[X1],Res).

upDownOne(Tam,[Y,Ini],Res):- findall([Y1,Ini],((Y1 is Y-1, Y1 >= 0);(Y1 is Y+1, Y1 =< Tam)), Res).

leftRightOne(Tam,[Ini,X],Res):- findall([Ini,X1],((X1 is X-1, X1 >= 0);(X1 is X+1, X1 =< Tam)), Res).

posValAjedrez(Tam,Ini,Res):- findall([X,Y],(
                                    (goUp2(Tam,Ini,R1)    , leftRightOne(Tam,R1,L1) , member([X,Y],L1));
                                    (goDown2(Tam,Ini,R2)  , leftRightOne(Tam,R2,L2) , member([X,Y],L2));
                                    (goLeft2(Tam,Ini,R3)  , upDownOne(Tam,R3,L3)    , member([X,Y],L3));
                                    (goRight2(Tam,Ini,R4) , upDownOne(Tam,R4,L4)    , member([X,Y],L4))), Res).

unPasoAjedrez(Tam, EstadoActual, EstSiguiente):- 
    posValAjedrez(Tam, EstadoActual, ListaEstadosSiguientes), 
    member(EstSiguiente, ListaEstadosSiguientes).

caminoAjedrez( _, 0, E, E, C, C ).
caminoAjedrez( Tam, Pasos, EstadoActual, EstadoFinal, CaminoHastaAhora, CaminoTotal):-
    Pasos > 0,
    unPasoAjedrez( Tam, EstadoActual, EstSiguiente),
    \+member(EstSiguiente, CaminoHastaAhora),
    PasosMenos1 is Pasos-1,
    caminoAjedrez( Tam, PasosMenos1, EstSiguiente, EstadoFinal, [EstSiguiente|CaminoHastaAhora], CaminoTotal ).

solucionOptimaAjedrez:-
    nat(N),                                         % Buscamos solucion de "coste" 0; si no, de 1, etc.
    caminoAjedrez(6, N, [0,0], [0,4], [[0,0]], C),  
    N1 is N+1, length(C,N1),                        
    reverse(CInvertido,C), 
    write("Optimal solution found with cost "), write(N1), write(": "), 
    write(CInvertido), nl, !.
                                
    
%Problema B3 finished

unPasoPuente( Time, [lado1,L1], [lado2,L2] ):- select(Time,L1,L2).
unPasoPuente( Time, [lado1,L1], [lado2,L2] ):- select(T1,L1,Laux), select(T2,Laux,L2), Time is max(T1,T2).

unPasoPuente( Time, [lado2,L1], [lado1,L2] ):- member(Time,[8,5,2,1]), \+member(Time,L1), sort([Time|L1],L2).
unPasoPuente( Time, [lado2,L1], [lado1,L2] ):- 
    member(T1,[8,5,2,1]), \+member(T1,L1),               
    member(T2,[8,5,2,1]), \+member(T2,L1), T1\=T2,
    sort([T1,T2|L1],L2), Time is max(T1,T2).

caminoPuente( 0, E,E, C,C ):-!.
caminoPuente( TiempoMax, EstadoActual, EstadoFinal, CaminoHastaAhora, CaminoTotal ):-
    unPasoPuente( Time, EstadoActual, EstSiguiente ),
    TiempoMaxReducido is TiempoMax-Time, TiempoMaxReducido >= 0,
    
    \+member(EstSiguiente,CaminoHastaAhora) ,
    
    
    caminoPuente( TiempoMaxReducido,EstSiguiente, EstadoFinal, [EstSiguiente|CaminoHastaAhora], CaminoTotal ).

solucionOptimaPuente:-
    nat(N),
    caminoPuente(N, [lado1,[1,2,5,8]],[lado2,[]],[[lado1,[1,2,5,8]]], C),                  
    reverse(CInvertido,C), 
    write("Optimal solution found with cost "), write(N), write(": "), nl, 
    write(CInvertido), nl, !.
