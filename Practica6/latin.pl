:- use_module(library(clpfd)).

example1:- latin([2,_,_,3,
 3,_,_,_,
 _,_,_,1,
 _,_,4,_]).
 
example2:- latin([5,3,_,_,7,_,_,_,_,
 6,_,_,1,9,5,_,_,_,
 _,9,8,_,_,_,_,6,_,
 8,_,_,_,6,_,_,_,3,
 4,_,_,8,_,3,_,_,1,
 7,_,_,_,2,_,_,_,6,
 _,6,_,_,_,_,2,8,_,
 _,_,_,4,1,9,_,_,5,
 _,_,_,_,8,_,_,7,9]).
 
latin(L):-
 length(L,Len), S is round(sqrt(Len)), Len is S*S, %% fail if Len is not a perfect square
 L ins 1..S,
 matrixByRows(S, L, Rows), constraintsFromSubLists(Rows),
 transpose(Rows,Cols), constraintsFromSubLists(Cols),
 label(L), nl,nl,displaySol(Rows), nl,nl, halt.
displaySol(Rows):- member(Row,Rows), nl, member(N,Row), write(N), write(' '), fail.
displaySol(_).

matrixByRows(_, [], []).
matrixByRows(S, L, [Row|Rows]):- append(Row,L1,L), length(Row,S), matrixByRows(S,L1,Rows).

constraintsFromSubLists([]).
constraintsFromSubLists([Row|Rows]):- all_different(Row), constraintsFromSubLists(Rows).
