:- use_module(library(clpfd)).

%% A (6-sided) "letter dice" has on each side a different letter.
%% Find four of them, with the 24 letters abcdefghijklmnoprstuvwxy such
%% that you can make all the following words: bake, onyx, echo, oval,
%% gird, smug, jump, torn, luck, viny, lush, wrap.

%Some helpful predicates:

word( [b,a,k,e] ).
word( [o,n,y,x] ).
word( [e,c,h,o] ).
word( [o,v,a,l] ).
word( [g,i,r,d] ).
word( [s,m,u,g] ).
word( [j,u,m,p] ).
word( [t,o,r,n] ).
word( [l,u,c,k] ).
word( [v,i,n,y] ).
word( [l,u,s,h] ).
word( [w,r,a,p] ).

num(X,N):- nth1( N, [a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,r,s,t,u,v,w,x,y], X ).

main:-
    length(D1,6), D1 ins 1..24,
    length(D2,6), D2 ins 1..24,
    length(D3,6), D3 ins 1..24,
    length(D4,6), D4 ins 1..24,
    
    append([D1,D2,D3,D4],Vars),
    all_distinct(Vars),
    
    findall(W,word(W),ListWords),
    declareConstraints(ListWords,D1,D2,D3,D4),
    
    labeling([ff],D1),
    labeling([ff],D2),
    labeling([ff],D3),
    labeling([ff],D4),
    
    writeN(D1), 
    writeN(D2), 
    writeN(D3), 
    writeN(D4), halt.
    
    
declareConstraints([],_,_,_,_).
declareConstraints([[W1,W2,W3,W4]|Words],D1,D2,D3,D4):-

    num(W1,N1),num(W2,N2),num(W3,N3),num(W4,N4),
    member(C1,D1),
    C1#=N1 #\/ C1#=N2 #\/ C1#=N3 #\/ C1#=N4,
    
    member(C2,D2),
    C2#=N1 #\/ C2#=N2 #\/ C2#=N3 #\/ C2#=N4,
    
    member(C3,D3),
    C3#=N1 #\/ C3#=N2 #\/ C3#=N3 #\/ C3#=N4,
    
    member(C4,D4),
    C4#=N1 #\/ C4#=N2 #\/ C4#=N3 #\/ C4#=N4,
    
    declareConstraints(Words,D1,D2,D3,D4).
    
    
writeN(D):- findall(X,(member(N,D),num(X,N)),L), write(L), nl, !.
