#include <iostream>
#include <stdlib.h>
#include <algorithm>
#include <vector>
using namespace std;

#define UNDEF -1
#define TRUE 1
#define FALSE 0
#define ConflictsToRefresh 1000

uint numVars;
uint numClauses;
vector<vector<int> > clauses;
vector<int> model;
vector<int> modelStack;
uint indexOfNextLitToPropagate;
uint decisionLevel;

int conflict = 0;
int conflictsTotal = 0;

vector< vector<int> > apariciones; //2*numVars size+1
vector< int > auxApa; //used for acceding ordered apariciones

vector< pair<int,int> > actividad; //numVars size
vector< int > auxAct; //used for acceding ordered actividad

int currentValueInModel(int lit){
    if (lit >= 0) return model[lit];
    else{
        if (model[-lit] == UNDEF) return UNDEF;
        else return 1 - model[-lit];
    }
}

void setLiteralToTrue(int lit){
    modelStack.push_back(lit);
    if (lit > 0) model[lit] = TRUE;
    else model[-lit] = FALSE;
}

int readClauses( ){
    char c = cin.get();
    while (c == 'c') {
        while (c != '\n') c = cin.get();
        c = cin.get();
    }  
    
    string aux;
    cin >> aux >> numVars >> numClauses;
    
    clauses.resize(numClauses);  
    model.resize(numVars+1,UNDEF);
    actividad.resize(numVars);
    auxAct.resize(numVars);
    apariciones.resize(2*numVars+1);
    auxApa.resize(2*numVars+1);
    
    for (uint i = 1; i < 2*numVars+1; ++i){
        if(i < numVars+1){
            pair<int,int> p (i-1,0);
            actividad[i-1] = p;
            auxAct[i-1] = i-1;
        }
        
        if(i < numVars+1) apariciones[i].push_back(i);
        else apariciones[i].push_back(-i+numVars);
    }
    
    int lit;
    for(uint i = 0; i < numClauses; ++i){
        while (cin >> lit and lit != 0) clauses[i].push_back(lit);
        if (clauses[i].size() == 1) {
            int lit = clauses[i][0];
            int val = currentValueInModel(lit);
            if (val == FALSE) return 10;
            else if (val == UNDEF) setLiteralToTrue(lit);
        }
        else{
            for(uint j = 0; j < clauses[i].size(); ++j){
                if(clauses[i][j] >= 0) apariciones[clauses[i][j]].push_back(i);
                else apariciones[-clauses[i][j]+numVars].push_back(i);
            }
        }
    }
    return 0;
}

void divideActividad(){
    for(uint i = 0; i < actividad.size(); ++i) actividad[i].second /= 2;
    conflict = 0;
}

void orderActividad(int i){ //swaps till get the position on its right place
    int j = i;
    while(j != 0 and actividad[--j].second <= actividad[i].second){
        swap(auxAct[actividad[i].first],auxAct[actividad[j].first]);
        swap(actividad[i],actividad[j]);
        --i;
    }
}

void addActividad(int lit){
    ++actividad[auxAct[abs(lit)-1]].second;
    orderActividad(auxAct[abs(lit)-1]);
}

bool propagateGivesConflict(){
    while(indexOfNextLitToPropagate < modelStack.size()){ 
        int indexToPropagate = -modelStack[indexOfNextLitToPropagate];
        if(indexToPropagate != 0){
            if(indexToPropagate < 0) indexToPropagate = -indexToPropagate+numVars;
            
            for (uint i = 1; i < apariciones[auxApa[indexToPropagate]].size(); ++i){
                bool someLitTrue = false;
                int numUndefs = 0;
                int lastLitUndef = 0;
                vector<int> literals;
                int clausula = apariciones[auxApa[indexToPropagate]][i];
                
                for (uint j = 0; not someLitTrue and j < clauses[clausula].size(); ++j){
                    int clausulai = clauses[clausula][j];
                    literals.push_back(clausulai);
                    int val = currentValueInModel(clausulai);
                    if (val == TRUE) someLitTrue = true;
                    else if (val == UNDEF){ ++numUndefs; lastLitUndef = clausulai;}
                }
                
                if (not someLitTrue){
                    if(numUndefs == 0){
                        for(uint k = 0; k < literals.size();++k) addActividad(literals[k]);
                        if(++conflict > ConflictsToRefresh) divideActividad();
                        ++conflictsTotal;
                        return true; // conflict! all lits false
                    }
                    
                    if(numUndefs == 1){
                        setLiteralToTrue(lastLitUndef);
                        for(uint k = 0; k < literals.size();++k){
                            if(lastLitUndef != literals[k]) addActividad(literals[k]);
                        }
                    }
                }
            }
        }
        ++indexOfNextLitToPropagate;
    }
    return false;
}

int getNextDecisionLiteral(){
    if(conflictsTotal > 2000){ //actividad
        for (uint i = 0; i < numVars; ++i)
            if (currentValueInModel(actividad[i].first+1) == UNDEF){
                return actividad[i].first+1; 
            }
    }
    else for (uint i = 0; i < 2*numVars; ++i) //occurs lists
        if (currentValueInModel(apariciones[i][0]) == UNDEF){
            return apariciones[i][0];  // returns first UNDEF var
        }
    return 0; // reurns 0 when all literals are defined
}

void backtrack(){
    uint i = modelStack.size() -1;
    int lit = 0;
    while (modelStack[i] != 0){
        lit = modelStack[i];
        model[abs(lit)] = UNDEF;
        modelStack.pop_back();
        --i;
    }
    modelStack.pop_back();
    --decisionLevel;
    indexOfNextLitToPropagate = modelStack.size();
    setLiteralToTrue(-lit);
}

void checkmodel(){
    for (uint i = 0; i < numClauses; ++i){
        bool someTrue = false;
        for (uint j = 0; not someTrue and j < clauses[i].size(); ++j){
            someTrue = (currentValueInModel(clauses[i][j]) == TRUE);
        }
        if (not someTrue){
            cout << "Error in model, clause is not satisfied:";
            for (uint j = 0; j < clauses[i].size(); ++j) cout << clauses[i][j] << " ";
            cout << endl;
            exit(1);
        }
    }  
}

bool order (const vector<int> & a, const vector<int> & b){ 
    return a.size() > b.size(); 
}

void orderApariciones(){
    sort(apariciones.begin(),apariciones.end(),order);  
    for(uint i = 0; i < 2*numVars; ++i){
        int a = apariciones[i][0]; //identificador
        if(a < 0) a = -a+numVars;
        auxApa[a] = i;
    }
}

int main(){ 
    int error = readClauses();
    if(error == 10){ cout << "UNSATISFIABLE" << endl; return error;}
    indexOfNextLitToPropagate = 0;  
    decisionLevel = 0;

    orderApariciones();
    
    // DPLL algorithm
    while(true){
        while(propagateGivesConflict()){
            if (decisionLevel == 0){ cout << "UNSATISFIABLE" << endl; return 10;}
            backtrack();
        }
        int decisionLit = getNextDecisionLiteral();
        if (decisionLit == 0){ checkmodel(); cout << "SATISFIABLE" << endl; return 20;}
        
        modelStack.push_back(0);
        ++indexOfNextLitToPropagate;
        ++decisionLevel;
        setLiteralToTrue(decisionLit);
    }
}
