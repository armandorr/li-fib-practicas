%Notacion: “dado N” significa que la variable N estara instanciada inicialmente. “Debe ser capaz de generar todas las respuestas posibles” significa que si hay backtracking debe poder generar la siguiente respuesta, como el nat(N) puede generar todos los naturales.

nat(0).
nat(N):- nat(N1), N is N1 + 1.

%1. (dificultad 1) Demuestra por induccion que son correctos los programas para pert con resto, long, permutacion y subcjto.

pert_r(X,[X|L],L).
pert_r(X,[Y|L],[Y|R]):- pert_r(X,L,R).

long([],0).
long([_|L],M):- long(L,N),M is N+1.

permutacion([],[]).
permutacion(L,[X|P]) :- pert_r(X,L,R), permutacion(R,P).

subcjto([],[]).  %subcjto(L,S) significa "S es un subconjunto de L".
subcjto([X|C],[X|S]):-subcjto(C,S).
subcjto([_|C],S):-subcjto(C,S).

%2. (dificultad 1) Escribe un predicado Prolog prod(L,P) que signifique “P es el producto de los elementos de la lista de enteros dada L”. Debe poder generar la P y tambien comprobar una P dada.

prod([X],X):-!.
prod([X|L],P):- prod(L,P1), P is P1*X.

%3. (dificultad 1) Escribe un predicado Prolog pescalar(L1,L2,P) que signifique “P es el producto escalar de los dos vectores L1 y L2”. Los dos vectores vienen dados por las dos listas de enteros L1 y L2. El predicado debe fallar si los dos vectores tienen una longitud distinta.

pescalar([],[],0).
pescalar([X|L1],[Y|L2],P):- pescalar(L1,L2,P1), P is P1 + (X*Y).

%4. (dificultad 2) Representando conjuntos con listas sin repeticiones, escribe predicados para las operaciones de interseccion y union de conjuntos dados.

union([],L,L).
union([X|L1],L2,P):- member(X,L2), !, union(L1,L2,P).
union([X|L1],L2,[X|P]):- union(L1,L2,P).

intersect([],_,[]).
intersect([X|L1],L2,[X|P]):- member(X,L2), !, intersect(L1,L2,P).
intersect([_|L1],L2,P):- intersect(L1,L2,P).

%5. (dificultad 2) Usando el concat, escribe predicados para el ultimo de una lista dada, y para el inverso de una lista dada.

ultimo(L,X):- append(_,[X],L).

inverse([],[]).
inverse(L,[X|L1]):- append(L2,[X],L),inverse(L2,L1).

%6. (dificultad 3) Escribe un predicado Prolog fib(N,F) que signifique “F es el N-esimo numero de Fibonacci para la N dada”. Estos numeros se definen como: fib(1) = 1, fib(2) = 1, y, si N > 2, como: fib(N) = fib(N - 1) + fib(N - 2).

fib(1,1).
fib(2,1).
fib(N,X):- N>2, N1 is N-1, fib(N1,X1),
           N2 is N-2, fib(N2,X2),
           X is X1+X2.

%7. (dificultad 3) Escribe un predicado Prolog dados(P,N,L) que signifique “la lista L expresa una manera de sumar P puntos lanzando N dados”. Por ejemplo: si P es 5, y N es 2, una solucion serıa [1,4]. (Notese que la longitud de L es N). Tanto P como N vienen instanciados. El predicado debe ser capaz de generar todas las soluciones posibles.

num(N):- between(1,6,N).

dados(0,0,[]).
dados(P,N,[X|L]):- N>0, num(X),
                   R is P-X, M is N-1,
                   dados(R,M,L).

%8. (dificultad 2) Escribe un predicado suma demas(L) que, dada una lista de enteros L, se satisface si existe algun elemento en L que es igual a la suma de los demas elementos de L, y falla en caso contrario.

sum([],0).
sum([X|L],S) :- sum(L,S1), S is S1+X.

suma_demas(L):- pert_r(X,L,R), suma(R,X), !.

%9. (dificultad 2) Escribe un predicado suma ants(L) que, dada una lista de enteros L, se satisface si existe algun elemento en L que es igual a la suma de los elementos anteriores a el en L, y falla en caso contrario.

suma_ants(L):- append(L1,[X|_],L), suma(L1,X), !.

%10. (dificultad 2) Escribe un predicado card(L) que, dada una lista de enteros L, escriba la lista que, para cada elemento de L, dice cuantas veces aparece este elemento en L. Por ejemplo, card( [1,2,1,5,1,3,3,7] ) escribira [[1,3],[2,1],[5,1],[3,2],[7,1]].

car([],[]).
car([X|L],[[X,N1]|Cr]):-car(L,C),pert_r([X,N],C,Cr),!,N1 is N+1.
car([X|L],[[X,1] |C]) :-car(L,C).

car(L):-car(L,C),write(C).

%11. (dificultad 2) Escribe un predicado Prolog esta ordenada(L) que signifique “la lista L de numeros enteros esta ordenada de menor a mayor”. Por ejemplo, con ?-esta ordenada([3,45,67,83]) dice yes. Pero con ?-esta ordenada([3,67,45]) dice no.

esta_ordenada([X,Y|L]):- !, X =< Y, esta_ordenada([Y|L]).
esta_ordenada([_]).
esta_ordenada([]).

%12. (dificultad 2) Escribe un predicado Prolog ordenacion(L1,L2) que signifique “L2 es la lista de enteros L1 ordenada de menor a mayor”. Por ejemplo: si L1 es [8,4,5,3,3,2], L2 sera [2,3,3,4,5,8]. Hazlo en una lınea, utilizando solo los predicados permutacion y esta ordenada.

perm_sort(L1,L2):- permutation(L1,L2), esta_ordenada(L2).

%13. (dificultad 2) ¿Que numero de comparaciones puede llegar a hacer en el caso peor el algoritmo de ordenacion basado en permutacion y esta ordenada?

    % n longitud lista
    % n! permutaciones + lineal comparar ==  n*n! < (n+1)! => Complejidad O((n+1)!)

%14. (dificultad 3) Escribe un predicado Prolog ordenacion(L1,L2) basado en el metodo de la insercion, usando un predicado insercion(X,L1,L2) que signifique: “L2 es la lista obtenida al insertar X en su sitio en la lista de enteros L1 que esta ordenada de menor a mayor”.

insertion(X,[],[X]):-!.
insertion(X,[Y|L1],[X,Y|L1]):- X =< Y,!.
insertion(X,[Y|L1],[Y|L2]):- insertion(X,L1,L2).

%15. (dificultad 2) ¿Que numero de comparaciones puede llegar a hacer en el caso peor el algoritmo de ordenacion basado en la insercion?

    % O(n^2) comparaciones  <= 1 + 2 + .. + n-1 = n(n-1)/2 = O(n^2)
    
%16. (dificultad 3) Escribe un predicado Prolog ordenacion(L1,L2) basado en el metodo de la fusion (merge sort): si la lista tiene longitud mayor que 1, con concat divide la lista en dos mitades, ordena cada una de ellas (llamada recursiva) y despues fusiona las dos partes ordenadas en una sola (como una “cremallera”). Nota: este algoritmo puede llegar a hacer como mucho n log n comparaciones (donde n es el tama˜no de la lista), lo cual es demostrablemente optimo.

split([],[],[]).
split([A],[A],[]).
split([A,B|R],[A|Ra],[B|Rb]) :-  split(R,Ra,Rb).

merge_sort([],[])   :- !.
merge_sort([X],[X]) :- !.
merge_sort(L,L3) :- split(L,L1,L2), merge_sort(L1,L11), merge_sort(L2,L22), merge(L11,L22,L3). 
  
merge(L, [], L) :- !.
merge([], L, L).
merge([X|L1],[Y|L2],[X|L3]) :- X=<Y, !, merge(L1,[Y|L2],L3).
merge([X|L1],[Y|L2],[Y|L3]) :- merge([X|L1],L2,L3).
    
%17. (dificultad 4) Escribe un predicado diccionario(A,N) que, dado un alfabeto A de sımbolos y un natural N, escriba todas las palabras de N sımbolos, por orden alfabetico (el orden alfabetico es segun el alfabeto A dado). Ejemplo: diccionario( [ga,chu,le],2) escribira: gaga gachu gale chuga chuchu chule lega lechu lele. Ayuda: define un predicado nmembers(A,N,L), que utiliza el pert para obtener una lista L de N sımbolos, escrıbe los sımbolos de L todos pegados, y provoca backtracking.

diccionario(A,N):-  nperts(A,N,S), escribir(S), fail.
diccionario(_,_).

nperts(_,0,[]):-!.
nperts(L,N,[X|S]):- subcjto(L,X), length(X,I), I is 1, N1 is N-1, nperts(L,N1,S).

escribir([]):-write(' '),nl,!.
escribir([[L1]|L]):- write(L1), escribir(L).

%18. (dificultad 3) Escribe un predicado palndromos(L) que, dada una lista de letras L, escriba todas las permutaciones de sus elementos que sean palındromos (capicuas). Ejemplo: palindromos([a,a,c,c]) escribe [a,c,c,a] y [c,a,a,c].

palindromos(L) :- permutacion(L,P), es_palindromo(P), write(P), nl, fail. 
palindromos(_). 

capicua([]).
capicua([_]) :- !.
capicua([X|L]) :- append(L1,[X],L), capicua(L1). 

%19. (dificultad 4) ¿Que 8 dıgitos diferentes tenemos que asignar a las letras S,E,N,D,M,O,R,Y, de manera que se cumpla la suma SEND+MORE=MONEY? Resuelve el problema en Prolog con un predicado suma que sume listas de dıgitos. El programa debe decir “no” si no existe solucion.

suma([],[],[],C,C).
suma([X1|L1],[X2|L2],[X3|L3],Cin,Cout) :-
    X3 is (X1 + X2 + Cin) mod 10,
    C  is (X1 + X2 + Cin) // 10,
    suma(L1,L2,L3,C,Cout).


send_more_money1 :-

    L = [S, E, N, D, M, O, R, Y, _, _],
    permutation(L, [0,1,2,3,4,5,6,7,8,9]),
    suma([D, N, E, S], [E, R, O, M], [Y, E, N, O], 0, M),

    write('S = '), write(S), nl,
    write('E = '), write(E), nl,
    write('N = '), write(N), nl,
    write('D = '), write(D), nl,
    write('M = '), write(M), nl,
    write('O = '), write(O), nl,
    write('R = '), write(R), nl,
    write('Y = '), write(Y), nl,
    write('  '), write([S,E,N,D]), nl,
    write('  '), write([M,O,R,E]), nl,
    write('-------------------'), nl,
    write([M,O,N,E,Y]), nl.

%20. (dificultad 4) Escribe el predicado simplifica que se ha usado con el programa de calcular derivadas.

simplifica(E,E1):- unpaso(E,E2),!, simplifica(E2,E1).
simplifica(E,E).

unpaso(A+B,A+C):- unpaso(B,C),!.
unpaso(B+A,C+A):- unpaso(B,C),!.
unpaso(A*B,A*C):- unpaso(B,C),!.
unpaso(B*A,C*A):- unpaso(B,C),!.
unpaso(0*_,0):-!.
unpaso(_*0,0):-!.
unpaso(1*X,X):-!.
unpaso(X*1,X):-!.
unpaso(0+X,X):-!.
unpaso(X+0,X):-!.
unpaso(N1+N2,N3):- number(N1), number(N2), N3 is N1+N2,!.
unpaso(N1*N2,N3):- number(N1), number(N2), N3 is N1*N2,!.
unpaso(N1*X+N2*X,N3*X):- number(N1), number(N2), N3 is N1+N2,!.
unpaso(N1*X+X*N2,N3*X):- number(N1), number(N2), N3 is N1+N2,!.
unpaso(X*N1+N2*X,N3*X):- number(N1), number(N2), N3 is N1+N2,!.
unpaso(X*N1+X*N2,N3*X):- number(N1), number(N2), N3 is N1+N2,!.

%21. (dificultad 4) Tres misioneros y tres canıbales desean cruzar un rıo. Solamente se dispone de una canoa que puede ser utilizada por 1 o 2 personas: misioneros o canıbales. Si los misioneros quedan en minorıa en cualquier orilla, los canıbales se los comeran. Escribe un programa Prolog que halle la estrategia para que todos lleguen sanos y salvos a la otra orilla.

inverso([],[]).
inverso(L,[X|L1]):- append(L2,[X],L), inverso(L2,L1).

mis:- camino( [lado1,3,3], [lado2,0,0], [[lado1,3,3]] ).

camino(Fin,Fin,Cam):- inverso(Cam,Sol), write(Sol), nl.
camino(Ini,Fin,Cam):- paso(Ini,E), novisitado(E,Cam), camino(E,Fin,[E|Cam]).

novisitado(E,Cam):- member(E,Cam), !,fail.
novisitado(_,_).

paso( [lado1,M1,C1], [lado2,M2,C2] ):- pasan(M,C), M2 is M1-M, C2 is C1-C, safe(M2,C2).
paso( [lado2,M1,C1], [lado1,M2,C2] ):- pasan(M,C), M2 is M1+M, C2 is C1+C, safe(M2,C2).

pasan(M,C):- member( [M,C], [ [0,1], [0,2], [1,0], [1,1], [2,0] ] ).

safe(M,C):- M>=0, M=<3, C>=0, C=<3, nocomen( M, C),
            M1 is 3-M,  C1 is 3-C,  nocomen(M1,C1).

nocomen(0,_).
nocomen(M,C):- M>=C.
                   
